<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Переводы</title>
		<link rel="stylesheet" type="text/css" href="/files/styles.css" />
		<script src="/files/js/jquery.min.js"></script>
		<script src="/files/js/runtime.js"></script>
    </head>
    <body>
		<div class="wrapper">
			<div class="content">
				<h2>Переводы</h2>
				<ul class="languages"></ul>
				<div class="language"></div>
			</div>

			<div class="forms">
				<div class="form">
					<h2>Добавление новой строки</h2>
					<form method="post" action="/action.php?mode=add_source">
						<div class="row">
							<label>
								Строка:
								<input type="text" maxlength="255" name="value" />
							</label>
						</div>
						<div class="row">
							<input type="submit" value="Добавить" />
						</div>
					</form>
				</div>

				<div class="form">
					<h2>Добавление новой локали</h2>
					<form method="post" action="/action.php?mode=add_locale">
						<div class="row">
							<label>
								Язык:
								<input type="text" maxlength="45" name="name" />
							</label>
						</div>

						<div class="row">
							<label>
								Языковой код (ru, en, es):
								<input type="text" maxlength="12" name="lang" />
							</label>
						</div>

						<div class="row">
							<input type="submit" value="Добавить" />
						</div>
					</form>
				</div>
			</div>
		</div>
    </body>
</html>
