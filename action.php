<?php

require_once 'protected/include/db.php';
require_once 'protected/include/CJSON.php';
require_once 'protected/include/Helpers.php';

$mode = Helpers::chkvar($_GET['mode']);
$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';

$data = array(
	'status' => 1,
	'show_message' => 1,
);

switch ($mode) {
	case 'add_locale':
		$name = Helpers::chkvar($_POST['name']);
		$lang = Helpers::chkvar($_POST['lang']);

		if (empty($name) || empty($lang)) {
			$data['status'] = 0;
			$data['message'] = 'Не задано одно из значений';
			break;
		}

		if ($lang == "default") {
			$data['status'] = 0;
			$data['message'] = 'Языковой код "default" занят';
			break;
		}

		$db = new db();
		if (!$db->insert('language', array(
					'lang' => $lang,
					'name' => $name,
				))
		) {
			$data['status'] = 0;
			$data['message'] = mysql_error();
			break;
		}

		$data['show_message'] = 0;
		$data['message'] = 'Новый язык добавлен';
		break;

	case 'delete_locale':
		$lang = Helpers::chkvar($_POST['lang']);

		if (empty($lang) || $lang == 'default') {
			$data['status'] = 0;
			$data['message'] = 'Ошибка удаления. Запись не найдена';
			break;
		}

		$db = new db();

		if (!$db->delete('language', "`lang` = '$lang'")) {
			$data['status'] = 0;
			$data['message'] = mysql_error();
			break;
		}

		$data['show_message'] = 0;
		$data['message'] = 'Язык удален';
		break;
		
	case 'add_source':
		$value = Helpers::chkvar($_POST['value']);

		if (empty($value)) {
			$data['status'] = 0;
			$data['message'] = 'Не задано значение';
			break;
		}

		$db = new db();
		if (!$db->insert('source', array('value' => $value))) {
			$data['status'] = 0;
			$data['message'] = mysql_error();
			break;
		}

		$data['message'] = 'Новая строка добавлена';
		break;

	case 'edit_source':
		$value = Helpers::chkvar($_POST['value']);
		$id = Helpers::chkvar($_POST['id']);

		if (empty($id)) {
			$data['status'] = 0;
			$data['message'] = 'Ошибка редактирования. Запись не найдена';
			break;
		}

		$db = new db();

		if (!$db->update('source', array('value' => $value), "`id` = '$id'")) {
			$data['status'] = 0;
			$data['message'] = mysql_error();
			break;
		}

		$data['message'] = 'Строка обновлена';
		break;

	case 'delete_source':
		$id = Helpers::chkvar($_POST['id']);

		if (empty($id)) {
			$data['status'] = 0;
			$data['message'] = 'Ошибка удаления. Запись не найдена';
			break;
		}

		$db = new db();

		if (!$db->delete('source', "`id` = '$id'")) {
			$data['status'] = 0;
			$data['message'] = mysql_error();
			break;
		}

		$data['show_message'] = 0;
		$data['message'] = 'Строка удалена';
		break;

	case 'edit_translate':
		$value = Helpers::chkvar($_POST['value']);
		$id = Helpers::chkvar($_POST['id']);
		$lang = Helpers::chkvar($_POST['lang']);

		if (empty($id) || empty($lang)) {
			$data['status'] = 0;
			$data['message'] = 'Ошибка редактирования. Запись не найдена';
			break;
		}

		$db = new db();
		$item = $db->select('translate', "`source_id` = '$id' and `lang` = '$lang'");
		if (empty($item)) {
			// Добавляем новый перевод
			if (!$db->insert('translate', array('source_id' => $id, 'lang' => $lang, 'value' => $value))) {
				$data['status'] = 0;
				$data['message'] = mysql_error();
				break;
			}
			
			$data['message'] = 'Перевод обновлен';
			break;
		}

		// Обновляем существующий перевод
		if (!$db->update('translate', array('value' => $value), "`source_id` = '$id' and `lang` = '$lang'")) {
			$data['status'] = 0;
			$data['message'] = mysql_error();
			break;
		}

		$data['message'] = 'Перевод обновлен';
		break;

	default:
		$data['message'] = 'Ничего не делаем';
		break;
}

if ($isAjax) {
	echo CJSON::encode($data);
} else {
	require('protected/views/nojs.php');
}