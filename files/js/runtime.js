$( document ).ready(function() {
	window.settings = {
		lang: 'default',
		page: 0
	};
	window.funset = {};
	
	window.funset.sourceload = function(){
		$.ajax({
			type: "GET",
			url: "/view.php?mode=get_source",
			success: function(data){
				$(".language").html(data);
			}
		});
	}
	
	window.funset.languagesload = function(){
		$.ajax({
			type: "GET",
			url: "/view.php?mode=get_languages",
			success: function(data){
				$(".languages").html(data);
				
				// active language refresh
				$(".languages li").removeClass("active");
				$(".languages .lang_" + window.settings.lang).addClass("active");
			}
		});
	}
	
	window.funset.languageload = function(){
		if(window.settings.lang == "default"){
			window.funset.sourceload();
		}else{
			$.ajax({
				type: "GET",
				url: "/view.php?mode=get_translate&lang=" + window.settings.lang,
				success: function(data){
					$(".language").html(data);
				}
			});
		}
	}
	
	window.funset.extractlang = function(el){
		var cl = el.attr("class").split(' ')[0];
		var l = "default";
		if(cl.substring(0, 5) == "lang_"){
			l = cl.replace("lang_", "");
		};
		return l;
	}
	
	window.funset.languageclick = function(){
		var el = $(this).parent();
		$(".languages li").removeClass("active");
		var cl = el.attr("class").split(' ')[0];
		if(cl.substring(0, 5) == "lang_"){
			window.settings.lang = cl.replace("lang_", "");
		}
		el.addClass("active");
		
		window.funset.languageload();
		return false;
	}
	
	// Submitting forms with ajax
	window.funset.formsubmit = function(callback){
		var el = $(this);
		var url = el.attr('action');
		var method = el.attr('method');
		
		$.ajax({
			type: method,
			url: url,
			dataType: "json",
			data: el.serialize(),
			success: function(data){
				if(data.status == 0){
					alert(data.message);
				}else{
					if(data.show_message){
						alert(data.message);
					}
					
					el[0].reset();
					
					if(callback && typeof callback == 'function'){
						callback();
					}
					
					window.funset.languagesload();
					window.funset.languageload();
				}
			}
		});
		return false;
	}
	
	window.funset.languagesload();
	window.funset.sourceload();
	$(".forms form").submit(window.funset.formsubmit);
});