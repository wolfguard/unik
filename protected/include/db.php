<?php

/**
 * Database helper
 *
 * @author Ivan Fedyaev <ivan.fedyaev@gmail.com>
 */
class db {

	public $host = 'localhost';
	public $dbname = 'site_unik';
	public $user = 'site_unik';
	public $password = 'site_unik';
	protected $connection;

	public function __construct() {
		$this->connection = mysql_connect($this->host, $this->user, $this->password)
				or die('MySql not detected');

		mysql_select_db($this->dbname, $this->connection)
				or die('Database unreacheable');
	}

	public function query($sql) {
		$result = mysql_query("$sql", $this->connection) or die(mysql_error());

		$rows = array();
		while ($row = mysql_fetch_array($result)) {
			$rows[] = $row;
		}

		return $rows;
	}

	/**
	 * Execute sql for insert and update operations
	 * @param string $sql
	 * @return bool false on error
	 */
	public function execute($sql) {
		$result = mysql_query("$sql", $this->connection);

		return $result;
	}
	
	public function select($table, $condition = null, $order = null){
		$sql = "SELECT * FROM `$table`";
		
		if(!empty($condition)){
			$sql .= " WHERE $condition";
		}
		
		if(!empty($order)){
			$sql .= " ORDER BY $order";
		}
		
		return $this->query($sql);
	}
	
	public function selectPage($table, $page, $pageSize = 15){
		
	}

	public function insert($table, $data) {
		$sql = "INSERT INTO $table (";
		foreach ($data as $k => $v) {
			$sql .= "`$k`,";
		}
		$sql = substr($sql, 0, -1);
		$sql .= ") values (";
		foreach ($data as $k => $v) {
			$sql .= "'$v',";
		}
		$sql = substr($sql, 0, -1);
		$sql .= ");";
		
		return $this->execute($sql);
	}
	
	public function update($table, $data, $condition = null) {
		$sql = "UPDATE $table SET ";
		foreach ($data as $k => $v) {
			$sql .= "`$k` = '$v',";
		}
		$sql = substr($sql, 0, -1);
		if(!empty($condition)){
			$sql .= " WHERE $condition";
		}
		$sql .= ";";
		
		return $this->execute($sql);
	}
	
	public function delete($table, $condition = null) {
		$sql = "DELETE FROM $table";
		
		if(!empty($condition)){
			$sql .= " WHERE $condition";
		}
		$sql .= ";";
		
		return $this->execute($sql);
	}

	public function __destruct() {
		mysql_close($this->connection);
	}

}

?>
