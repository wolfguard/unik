CREATE  TABLE `language` (
	`lang` VARCHAR(12) NOT NULL,
	`name` VARCHAR(45) NOT NULL,
	PRIMARY KEY (`lang`)
);

CREATE  TABLE `source` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`value` VARCHAR(255) NULL,
	PRIMARY KEY (`id`),
	INDEX `value` (`value` ASC)
);

CREATE  TABLE `translate` (
	`source_id` INT NOT NULL,
	`lang` VARCHAR(12) NOT NULL,
	`value` TEXT NULL,
	PRIMARY KEY (`source_id`, `lang`),
	INDEX `fk_translate_lang_idx` (`lang` ASC),
	CONSTRAINT `fk_translate_lang`
		FOREIGN KEY (`lang` ) REFERENCES `language` (`lang`)
		ON DELETE CASCADE ON UPDATE CASCADE,
	INDEX `fk_translate_source_id_idx` (`source_id` ASC),
	CONSTRAINT `fk_translate_source_id`
		FOREIGN KEY (`source_id` ) REFERENCES `source` (`id`)
		ON DELETE CASCADE ON UPDATE CASCADE
);