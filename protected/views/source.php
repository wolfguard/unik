<?php foreach ($data as $item): ?>
	<div class="row">
		<form method="post" action="/action.php?mode=edit_source">
			<input name="id" type="hidden" value="<?= $item['id'] ?>" />
			<label>
				Строка:
				<input type="text" maxlength="255" name="value" value="<?= $item['value'] ?>" />
			</label>
			<input type="submit" value="Изменить" />
		</form>
		
		<form method="post" action="/action.php?mode=delete_source">
			<input name="id" type="hidden" value="<?= $item['id'] ?>" />
			<input type="submit" value="Удалить" />
		</form>
	</div>
<?php endforeach; ?>
<script>
	$(".content .language form").submit(window.funset.formsubmit);
</script>
