<li class="lang_default<?= !empty($lang) && $lang != 'default' ? '' : ' active'; ?>"><a href="#">Default</a></li>
<?php foreach ($data as $item): ?>
	<li class="lang_<?= $item['lang'] ?>">
		<a href="#"><?= $item['name'] ?></a>
		<form method="post" action="/action.php?mode=delete_locale">
			<input type="hidden" name="lang" value="<?= $item['lang'] ?>" />
			<input type="submit" value="d" />
		</form>
	</li>
<?php endforeach; ?>
<script>
	$(".languages li a").click(window.funset.languageclick);
	$(".languages form").submit(function(){
		var el = $(this).parent();
		var l = window.funset.extractlang(el);
		
		window.funset.formsubmit.call(this, function(){
			if(l == window.settings.lang){
				window.settings.lang = "default";
			}
		});
		return false;
	});
</script>