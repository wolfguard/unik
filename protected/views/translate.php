<?php foreach ($data as $item): ?>
	<div class="row">
		<form method="post" action="/action.php?mode=edit_translate">
			<input name="id" type="hidden" value="<?= $item['id'] ?>" />
			<input name="lang" type="hidden" value="<?= $lang ?>" />
			<div class="source-string"><?= $item['svalue'] ?></div>
			<label>
				Перевод:
				<input type="text" maxlength="255" name="value" value="<?= $item['value'] ?>" />
			</label>
			<input type="submit" value="Изменить" />
		</form>
	</div>
<?php endforeach; ?>
<script>
	$(".content .language form").submit(window.funset.formsubmit);
</script>