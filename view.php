<?php

require_once 'protected/include/db.php';
require_once 'protected/include/Helpers.php';

$mode = Helpers::chkvar($_GET['mode']);

switch($mode){
	case 'get_source':
		$db = new db();
		$data = $db->select('source');
		require('protected/views/source.php');
		
		break;
	
	case 'get_languages':
		$lang = Helpers::chkvar($_GET['lang']);
		
		$db = new db();
		$data = $db->select('language');
		require('protected/views/languages.php');
		
		break;
	
	case 'get_translate':
		$lang = Helpers::chkvar($_GET['lang']);
		if(empty($lang) || $lang == 'default')
		{
			echo 'Ошибка подгрузки переводов';
			break;
		}
		
		$db = new db();
		$data = $db->query("SELECT s.id, t.lang, s.value as svalue, t.value FROM `source` as s left join `translate` as t on t.source_id = s.id and t.lang = '$lang';");
		require('protected/views/translate.php');
		
		break;
}